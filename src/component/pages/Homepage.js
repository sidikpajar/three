import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';

const instance = axios.create({
  baseURL: 'https://swapi.dev/api/',
  timeout: 1000,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
  }
});


class Homepage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      people:[],
      films:[],
    }
  }
  
  async componentDidMount(){
    await this.getData();
  }

  getData = async () =>{
    instance({
      method: 'get',
      url: `people/1/`,
      data: {},
      validateStatus: () => true,
    })
    .then(response => {
      this.setState({
        people:response.data
      })
      if(response.data.films !== undefined){
        for(let x=0; x < response.data.films.length; x++){
          this.getFilms(response.data.films[x])
        }
      }
      console.log(response.data)
    })
    .catch(errors => {
    })
  }

  getFilms = async (data) => {
    const { films } = this.state
    instance({
      method: 'get',
      url:`${data}`,
      data: {},
      validateStatus: () => true,
    })
    .then(response => {
      films.push(response.data.title); 
    })
    .catch(errors => {
    })
  }

  NumberFilms() {
    const { films } = this.state
    return (
      <div>
        {films.map((number) =>
          <div key={number.toString()} className="age">Title: {number}</div>
        )}
      </div>
    );
  }

  render() {
    const { people } = this.state
    return (
      <Container style={{marginTop:100}}>
        <Grid container>
          <Paper>
            <Grid item className="paperStyle" sm={12} xs={12}>
              <div className="detail-wrapping">
                <div className="name">{`${people.name}`}</div>
                <div className="age">Gender: {people.gender}</div>
                <div className="age">Birth Year:{people.birth_year}</div>
                <div className="age">Eye Color:{people.eye_color}</div>
                <div className="age">Skin Color:{people.skin_color}</div>
                <div className="age">Height:{people.height}</div>
                <div className="age">Mass:{people.mass}</div>
              </div>
            </Grid>
          </Paper>
        </Grid>
        <Grid container style={{marginTop:20}}>
          <Paper>
            <Grid item className="paperStyle" sm={12} xs={12}>
              <div className="detail-wrapping">
                <div className="name">Films</div>
                {this.NumberFilms()}
              </div>
            </Grid>
          </Paper>
        </Grid>
      </Container>
    )
  }
}

export default Homepage;
