import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import './styles/index.scss';
import Homepage from './component/pages/Homepage';

import {
  BrowserRouter as Router, Switch, Route
} from 'react-router-dom';

function App() {
  return (
    <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/" exact component={Homepage} />
          </Switch>
        </Router>
    </Provider>
  );
}

export default App;
